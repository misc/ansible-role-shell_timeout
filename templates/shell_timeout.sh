#!/bin/bash
# {{ ansible_managed }}
# code from abrt-console-notification.sh
# verify if there is a tty
tty -s || return 0

# check if the shell is interactive
[ -z "$PS1" ] && return 0

# do not remove for system user
if [ -z "$HOME" ]; then
    return 0
fi

export TMOUT={{ timeout }} 
